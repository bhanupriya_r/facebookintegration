package com.zandig.facebook.integration.test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FacebookTestCase extends CamelSpringTestSupport{

	@Override
	protected AbstractApplicationContext createApplicationContext() {
		return new ClassPathXmlApplicationContext("camel-context.xml");
	}
	
	@Test
	public void testFacbookDetails() throws UnsupportedEncodingException, InterruptedException{
		String token = "729316460518738|CAACEdEose0cBAIoLsqRzlBhfFWVSKSbBQnbcB8hl1z9e0gP9qLAKYLDghxjbGZCpPTur4CZBcKpf8KlZC3P5W3l1ZBTq0SGg9IX0ZAYAuEWLHRD5zh8ZCf2aALSyzHewCaZCEZBHmwMqtTTztUaSUr4r6SNiEDcofzrnfGHLEl2uj4PYJ4BCkGEcNEY3IWRuopf2C0ereu4NxwBCZAbyv79FZC";
		System.out.println(token);
		token = URLEncoder.encode(token, "UTF-8");
		MockEndpoint mEnd = getMockEndpoint("mock:finish");
		mEnd.setExpectedCount(1);
		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("AccessToken", token);
		sendBody("direct:start", null, headers);
		mEnd.assertIsSatisfied();
	}

}
